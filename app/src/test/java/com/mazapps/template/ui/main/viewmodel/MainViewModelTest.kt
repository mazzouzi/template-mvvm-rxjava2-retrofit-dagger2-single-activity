package com.mazapps.template.ui.main.viewmodel

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mazapps.tabesto.ui.main.viewmodel.fromJson
import com.mazapps.template.api.RetrofitApiService
import com.mazapps.template.data.AppDataManager
import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.data.model.Wikipedia
import com.mazapps.template.di.component.DaggerTestApplicationComponent
import com.mazapps.template.di.module.TestApplicationModule
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Matchers.anyString
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response
import javax.inject.Inject

/**
 * @author morad.azzouzi on 12/11/2020.
 */
class MainViewModelTest {

    @Inject
    lateinit var retrofitApiService: RetrofitApiService
    @Inject
    lateinit var dataManager: AppDataManager

    private var stateSubscriber: TestObserver<CallStateEnum> = TestObserver.create()
    private lateinit var viewModel: MainViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        val component = DaggerTestApplicationComponent
            .builder()
            .applicationModule(TestApplicationModule())
            .build()
        component.inject(this)

        viewModel = MainViewModel(dataManager = dataManager)
    }

    @Test
    fun test_getKeywordCount_success() {
        val type = object : TypeToken<Wikipedia>() {}.type
        val bodyResponse: Wikipedia = Gson().fromJson(
            "wikipedia.json",
            javaClass.classLoader!!,
            type
        )
        val response = Response.success(bodyResponse)

        // Given
        Mockito
            .`when`(
                retrofitApiService.getCount(anyString(), anyString(), anyString(), anyString())
            )
            .thenReturn(Single.just(response))

        // When
        viewModel.state.subscribe(stateSubscriber)
        viewModel.getKeywordCount()
        viewModel.state.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.SUCCESS,
            CallStateEnum.IDLE
        )
    }

    @Test
    fun test_getKeywordCount_error() {
        // Given
        Mockito
            .`when`(
                retrofitApiService.getCount(anyString(), anyString(), anyString(), anyString())
            )
            .thenReturn(Single.error(RuntimeException()))

        // When
        viewModel.state.subscribe(stateSubscriber)
        viewModel.getKeywordCount()
        viewModel.state.onComplete()

        // Then
        stateSubscriber.awaitTerminalEvent()
        stateSubscriber.assertValues(
            CallStateEnum.IDLE,
            CallStateEnum.IN_PROGRESS,
            CallStateEnum.ERROR,
            CallStateEnum.IDLE
        )
    }
}