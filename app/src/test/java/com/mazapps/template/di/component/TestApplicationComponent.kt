package com.mazapps.template.di.component

import com.mazapps.template.di.module.ApplicationModule
import com.mazapps.template.ui.main.viewmodel.MainViewModelTest
import dagger.Component
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 12/11/2020.
 */
@Singleton
@Component(modules = [ApplicationModule::class])
interface TestApplicationComponent : ApplicationComponent {

    fun inject(mainViewModelTest: MainViewModelTest)
}