package com.mazapps.template.di.module

import com.mazapps.template.api.RetrofitApiService
import com.mazapps.template.data.AppDataManager
import com.mazapps.template.data.DataManager
import retrofit2.Retrofit
import org.mockito.Mockito

/**
 * @author morad.azzouzi on 12/11/2020.
 */
class TestApplicationModule : ApplicationModule() {

    override fun provideDataManager(appDataManager: AppDataManager): DataManager {
        return Mockito.mock(AppDataManager::class.java)
    }

    override fun provideApiService(retrofit: Retrofit): RetrofitApiService {
        return Mockito.mock(RetrofitApiService::class.java)
    }
}