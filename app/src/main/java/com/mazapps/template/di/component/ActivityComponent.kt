package com.mazapps.template.di.component

import com.mazapps.template.ui.main.view.MainActivity
import com.mazapps.template.di.PerActivity
import com.mazapps.template.di.module.ActivityModule
import dagger.Component

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@PerActivity
@Component(dependencies = [ApplicationComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)
}