package com.mazapps.template.di.module

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.mazapps.template.data.DataManager
import com.mazapps.template.di.PerActivity
import com.mazapps.template.ui.main.viewmodel.MainViewModel
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @Provides
    fun provideItemDecorator() = DividerItemDecoration(activity, RecyclerView.VERTICAL)

    @Provides @PerActivity
    fun provideCompositeDisposable(): CompositeDisposable = CompositeDisposable()

    @Provides @PerActivity
    fun provideMainViewModel(dataManager: DataManager): MainViewModel = MainViewModel(dataManager)

    /*
      /!\ Bien penser a rajouter des @PerActivity aux objets qui seront instanciés par Activity /!\
     */
}