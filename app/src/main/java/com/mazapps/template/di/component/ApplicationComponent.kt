package com.mazapps.template.di.component

import com.mazapps.template.data.DataManager
import com.mazapps.template.di.module.ApplicationModule
import dagger.Component
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@Singleton
@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {

    /**
     *  ApplicationComponent est une dépendance de ActivityComponent
     *  ActivityModule a besoin du DataManager alors ApplicationComponent doit l'exposer
     */
    fun getDataManager(): DataManager
}