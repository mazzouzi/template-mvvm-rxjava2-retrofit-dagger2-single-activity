package com.mazapps.template.di

import javax.inject.Scope

/**
 * @author morad.azzouzi on 11/11/2020.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity