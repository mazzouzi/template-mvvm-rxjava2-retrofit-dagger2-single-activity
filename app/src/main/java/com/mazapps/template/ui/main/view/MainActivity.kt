package com.mazapps.template.ui.main.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.mazapps.template.MyApplication
import com.mazapps.template.R
import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.di.component.ActivityComponent
import com.mazapps.template.di.component.DaggerActivityComponent
import com.mazapps.template.di.module.ActivityModule
import com.mazapps.template.ui.main.viewmodel.MainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    lateinit var activityComponent: ActivityComponent

    @Inject lateinit var viewModel: MainViewModel
    @Inject lateinit var disposables: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        initDagger()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        observeState()
    }

    private fun initDagger() {
        activityComponent = DaggerActivityComponent
            .builder()
            .applicationComponent((application as MyApplication).applicationComponent)
            .activityModule(ActivityModule(this))
            .build()
            .also {
                it.inject(this)
            }
    }

    private fun observeState() {
        disposables.add(
            viewModel
                .state
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        CallStateEnum.SUCCESS -> onSuccess()
                        CallStateEnum.ERROR -> onError()
                        else -> {
                            // do nothing
                        }
                    }
                }
        )
    }

    private fun onSuccess() {
        count.text = viewModel.count.toString()
    }

    private fun onError() {
        Toast.makeText(this, "an error occured", Toast.LENGTH_SHORT).show()
    }

    override fun onStart() {
        super.onStart()
        disposables.add(viewModel.getKeywordCount())
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }
}