package com.mazapps.template

import android.app.Application
import com.mazapps.template.di.component.ApplicationComponent
import com.mazapps.template.di.component.DaggerApplicationComponent

/**
 * @author morad.azzouzi on 11/11/2020.
 */
class MyApplication : Application() {

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.create()
    }
}